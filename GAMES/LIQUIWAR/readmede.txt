Liquid War ist ein einzigartiges Kriegsspiel f�r mehrere Spieler. Die 
Regeln sind wahrhaft neuartig und wurden von Thomas Colcombet entwickelt. 
Man steuert eine fl�ssige Armee und muss versuchen die Gegner aufzufressen. 
Es gibt einen Einzelspielermodus, aber das Spiel ist eindeutig auf mehrere 
Spieler ausgelegt und unterst�tzt das Spielen �ber Netzwerk.

Homepage     : http://www.ufoot.org/liquidwar/v5
Autor        : ufoot@ufoot.org
Mailingliste : http://mail.nongnu.org/mailman/listinfo/liquidwar-user (*)

(*) auf Englisch!


----------- W I C H T I G E     I N F O R M A T I O N E N ------------

Liquid War is ein Kriegspiel f�r mehrere.
Copyright (C) 1998-2007 Christian Mauduit (ufoot@ufoot.org)

Dieses Programm ist freie Software. Sie k�nnen es unter den
Bedingungen der GNU General Public License, wie von der Free
Software Foundation ver�ffentlicht, weitergeben und/oder
modifizieren, entweder gem�� Version 2 der Lizenz oder (nach Ihrer
Option) jeder sp�teren Version.

Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung, da�
es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar
ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT
F�R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General
Public License.

Sie sollten eine Kopie der GNU General Public License zusammen mit
diesem Programm erhalten haben. Falls nicht, schreiben Sie an die
Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
MA  02110-1301, USA.
