Liquid War est un "wargame" multijoueur in�dit. Ses r�gles sont vraiment
originales et ont �t� invent�es par Thomas Colcombet. L'id�e est de 
contr�ler une arm�e de liquide et d'essayer de "manger" ses adversaires.
Il est possible de jouer seul, mais le jeux est con�u pour se jouer � 
plusieurs, un mode r�seau �tant disponible.

Site web de Liquid War  : http://www.ufoot.org/liquidwar/v5
Pour contacter l'auteur : ufoot@ufoot.org

Si vous avez des questions ou des remarques � propos de Liquid War,
vous pouvez obtenir de l'aide et des infos sur la liste de diffusion
"Liquid War user", mais attention, on y parle anglais :

http://mail.nongnu.org/mailman/listinfo/liquidwar-user

Bonne journ�e,

U-Foot


-------------- I N F O R M A T I O N S     L E G A L E S -------------

Liquid War est un "wargame" multijoueur.
Copyright (C) 1998-2007 Christian Mauduit (ufoot@ufoot.org)

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le 
modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, 
telle que publi�e par la Free Software Foundation ; version 2 de la licence, 
ou encore (� votre choix) toute version ult�rieure.
	          
Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE 
GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou 
D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence 
Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en 
m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free 
Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
